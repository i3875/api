// Get env data
require('dotenv').config({ path: './.env' })

module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      database: process.env.DATABASE_MYSQL,
      user: process.env.USER_DB_MYSQL,
      password: process.env.PASSWORD_DB_MYSQL,
      host: process.env.HOST_DB_MYSQL
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './src/db/migrations'
    },
    seeds: {
      directory: './src/db/seeds'
    }
  },

  production: {
    client: 'mysql2',
    connection: {
      database: process.env.DATABASE_MYSQL,
      user: process.env.USER_DB_MYSQL,
      password: process.env.PASSWORD_DB_MYSQL,
      host: process.env.HOST_DB_MYSQL
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: './src/db/migrations'
    },
    seeds: {
      directory: './src/db/seeds'
    }
  }
}
