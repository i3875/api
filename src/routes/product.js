// Usecases
const product = require('../usecases/product')

module.exports = (router) => {
  router.get('/product/', async (ctx, next) => {
    try {
      let payload = await product.getListProduct()
      ctx.body = {
        success: true,
        ...payload
      }
    } catch (error) {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: error.message
      }
    }
  })
  router.get('/product/:idProducto', async (ctx, next) => {
    try {
      const { idProducto } = ctx.params
      let payload = await product.getProductId(idProducto)
      ctx.body = {
        success: true,
        ...payload
      }
    } catch (error) {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: error.message
      }
    }
  })
  router.post('/product/', async (ctx, next) => {
    try {
      let payload = await product.saveProduct(ctx.request.body)
      ctx.body = {
        success: true,
        ...payload
      }
    } catch (error) {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: error.message
      }
    }
  })
  router.put('/product/:idProducto', async (ctx, next) => {
    try {
      const { idProducto } = ctx.params
      let payload = await product.updateProduct(ctx.request.body, idProducto)
      ctx.body = {
        success: true,
        ...payload
      }
    } catch (error) {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: error.message
      }
    }
  })
  router.put('/product/delect/:idProducto', async (ctx, next) => {
    try {
      const { idProducto } = ctx.params
      let payload = await product.delectProduct(idProducto)
      ctx.body = {
        success: true,
        ...payload
      }
    } catch (error) {
      ctx.status = 400
      ctx.body = {
        success: false,
        error: error.message
      }
    }
  })
}
