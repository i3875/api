module.exports = Object.freeze({
  PRODUCT: {
    ERROR_LISTPRODUCT: 'Error al mostrar la lista de productos',
    ERROR_GETPRODUCTID: 'Error al mostrar el productos',
    ERROR_SAVEPRODUCT: 'Error al guardar el producto',
    ERROR_UPDATEPRODUCT: 'Error al actualizar el producto',
    ERROR_DELECTPRODUCT: 'Error al borrar el producto'
  }
})
