module.exports = Object.freeze({
  PRODUCT: {
    MESSAGE_SAVEPRODUCT: 'Se guardo correctamente el producto',
    MESSAGE_UPDATEPRODUCT: 'Se actualizo correctamente el producto',
    MESSAGE_DELECTPRODUCT: 'Se elimino correctamente el producto'
  }
})
