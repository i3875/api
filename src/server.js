// Get server data
const Koa = require('koa')
const cors = require('kcors')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const koaBody = require('koa-body')
const serve = require('koa-static-server')

// Initialize server
const app = new Koa()
const router = new Router()

// Env configuration
const environment = process.env.NODE_ENV || 'development'

// Initialize common router
router.get('/', async (ctx, next) => {
  // Asignación de mensaje
  ctx.body = 'Api V1'
})

// End Points
require('./routes/product')(router)

// Review current instance
if (environment === 'production') {
  // Message
  console.log('Initializing helmet...')
  // Get helmet data
  const helmet = require('koa-helmet')
  // Use middleware
  app.use(helmet())
}

// Use common middleware
app.use(cors())
app.use(koaBody({multipart: true, formidable: {maxFileSize: 10000000}}))
app.use(bodyParser())
// Use routers.
app.use(router.routes()).use(router.allowedMethods())
app.use(serve({rootDir: 'public', rootPath: '/public'}))

// Show error
app.on('error', (err, ctx) => {
  console.error(`[ERROR] in (${ctx.path}): `, err)
})

// Export current instance
module.exports = app
