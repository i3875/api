const { baseMySql } = require('../db/knex')
const moment = require('moment')
const ERROR = require('../constants/error')
const MESSAGE = require('../constants/messages')

module.exports = {
  async getListProduct () {
    try {
      const response = await baseMySql.select([
        'id',
        'name',
        'descripcion',
        'price',
      ])
        .from('producto')
        .where('active', true)
        .orderBy('created_at', 'desc')
      return response
    } catch (error) {
      throw new Error(ERROR.PRODUCT.ERROR_LISTPRODUCT)
    }
  },
  async getProductId (id = 0) {
    try {
      const response = await baseMySql.first([
        'id',
        'name',
        'descripcion',
        'price',
      ])
        .from('producto')
        .where('active', true)
        .andWhere('id', id)
      return response
    } catch (error) {
      throw new Error(ERROR.PRODUCT.ERROR_GETPRODUCTID)
    }
  },
  async saveProduct (body) {
    try {
      const [ id ] = await baseMySql('producto')
      .returning('id')
      .insert({
        'name': body.name,
        'descripcion': body.descripcion,
        'price': body.price,
        'created_at': moment()
      })

      return {
        message: MESSAGE.PRODUCT.MESSAGE_SAVEPRODUCT,
        idProduct: id
      }
    } catch (error) {
      throw new Error(ERROR.PRODUCT.ERROR_SAVEPRODUCT)
    }
  },
  async updateProduct (body, id = 0) {
    try {
      await baseMySql('producto')
      .where('id', id)
      .update({
        'name': body.name,
        'descripcion': body.descripcion,
        'price': body.price
      })
      return {
        message: MESSAGE.PRODUCT.MESSAGE_UPDATEPRODUCT,
      }
    } catch (error) {
      throw new Error(ERROR.PRODUCT.ERROR_UPDATEPRODUCT)
    }
  },
  async delectProduct (id = 0) {
    try {
      await baseMySql('producto')
      .where('id', id)
      .update({
        'active': false
      })
      return {
        message: MESSAGE.PRODUCT.MESSAGE_DELECTPRODUCT,
      }
    } catch (error) {
      throw new Error(ERROR.PRODUCT.ERROR_DELECTPRODUCT)
    }
  }
}
