// Get env data
require('dotenv').config({path: '../../.env'})
// MySQL config
const USER_DB_MYSQL = process.env.USER_DB_MYSQL
const PASSWORD_DB_MYSQL = process.env.PASSWORD_DB_MYSQL
const DATABASE_MYSQL = process.env.DATABASE_MYSQL
const HOST_DB_MYSQL = process.env.HOST_DB_MYSQL

// Configuring DB Instances
module.exports = {
  // MYSQL
  baseMySql: require('knex')({
    client: 'mysql2',
    connection: {
      host: HOST_DB_MYSQL,
      user: USER_DB_MYSQL,
      password: PASSWORD_DB_MYSQL,
      database: DATABASE_MYSQL
    }
  })
}
