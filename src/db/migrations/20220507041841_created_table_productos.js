exports.up = async (knex) => {
  await knex.schema.createTableIfNotExists('producto', (table) => {
    table.increments('id')
    table.string('name', 40)
    table.string('descripcion', 70)
    table.decimal('price').notNullable();
    table.datetime('created_at').notNullable().defaultTo(knex.fn.now())
    table.boolean('active').notNullable().defaultTo(true)
  })
}

exports.down = async (knex) => {
  await knex.schema.dropTable('producto')
}

