# API

## Usage



Use development enviroment
```
npm run dev
```
---

Stack

* NodeJS
* Knex
* Mysql SQL
* ES6
* NPM

Dependencies

* Koa

---

Para crear una migración

```
npm run migration:make migration_name
```


Aplicar un seed en especifico

```
npm run migration:run
```
