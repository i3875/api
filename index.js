// Get env data
require('dotenv').config()
// Get server configuration
const server = require('./src/server')
// Initialize port
const port = process.env.PORT || 9000
// Listen port
server.listen(port, function () {
  // Show message
  console.log(`Server run: http://localhost:${port}`)
})
